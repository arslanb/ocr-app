# README #

This is a web app. You have to place it in a web server. This App take an image with text and reads the text from the image and prints on the screen.

### What is this repository for? ###

This repository is for OCR web app for text reader from the given image.

### How do I get set up? ###

This needs a web server which have installed a PHP. The required configurations for PHP are below:

PHP >= 5.5.9
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension

**Note:**
We also need need to install tesseract library on system side

Here is the URL:
https://github.com/tesseract-ocr/tesseract/wiki

**For Linux:**
sudo apt-get install tesseract-ocr

**For Windows:**
http://digi.bib.uni-mannheim.de/tesseract/tesseract-ocr-setup-3.05.00dev.exe

Make sure this installer install the program to C:\Tesseract-OCR

No need of database for this app.

You simply need to run this app to the path to this project's public directory.

[path to the folder]/public

ocr-app/public


Owner:
Arslan B