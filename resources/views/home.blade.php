<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Bootstrap -->
        <link href="{{asset('assets/vendor/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/styles.css')}}" rel="stylesheet" type="text/css">

        <script src="{{asset('assets/vendor/jquery/dist/jquery.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="header">
                        <h2>Upload image/PDF from which you want to read text</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Select File:</label>
                            <div class="upload-field-wrapper">
                                <input class="form-control custom-file-uplaod" placeholder="Choose File"
                                       disabled="disabled"/>
                                <div class="fileUpload btn btn-default">
                                    <span>Browse...</span>
                                    <input class="upload-btn upload" name="ocr_image" type="file"/>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-sm" type="submit">Upload</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <script type="text/javascript">
        $(".upload-btn").change(
            function () {
                $(this).parents('.upload-field-wrapper').find('.custom-file-uplaod').val($(this).val());
            }
        );
    </script>
    </body>
</html>
