<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Bootstrap -->
        <link href="{{asset('assets/vendor/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/styles.css')}}" rel="stylesheet" type="text/css">

        <script src="{{asset('assets/vendor/jquery/dist/jquery.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="header">
                        <h2>The text from your image file is below:</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12"><button class="btn btn-primary pull-right"><a href="{{url('/')}}">Back</a></button></div>
                <div class="col-xs-6">
                    @if(@empty($text))
                        <h3>Image contain no text</h3>
                    @else
                        <pre>{{$text}}</pre>
                    @endif
                </div>
                <div class="col-xs-6">
                    <img src="{{$image}}" />
                </div>
            </div>
        </div>
    </body>
</html>
