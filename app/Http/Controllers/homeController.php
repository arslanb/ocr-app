<?php

/**
 * Controller File for Home Controller
 * Author: Arslan B
 */

namespace App\Http\Controllers;

use illuminate\Http\Request;
use TesseractOCR;

/**
 *
 */

class HomeController extends Controller
{
    /**
     * Path to the tesseract executable and directory
     */
    private $tesseract_exe = 'C:\Tesseract-OCR\tesseract.exe';
    private $tesseract_exe_dir = 'C:\Tesseract-OCR';


    /**
     * To handle form post request for image upload and get image text
     * Public function
     * @param request @var to get request data
     * @return view with image text
     **/
    public function showImageText(Request $request){

        /**
         * Handling request without file.
         */

        if(!$request->file('ocr_image')){
            return back();
        }

        /**
         * storing uploaded file to server
         */
        $request->file('ocr_image')->move('uploads', $request->file('ocr_image')->getClientOriginalName());
        $image = 'uploads\\'.$request->file('ocr_image')->getClientOriginalName();


        /**
         * Using Tesseract OCR Library
         */
        $OCR = new TesseractOCR($image);
        $OCR->executable($this->tesseract_exe);
        $OCR->tessdataDir($this->tesseract_exe_dir);
        $OCR->psm(11);
        $text = $OCR->run();
        /**
         * Returning view with the image text data
         */

        return view('ocr_results')->with('text', $text)->with('image', $image);
    }
}